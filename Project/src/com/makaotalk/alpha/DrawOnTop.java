package com.makaotalk.alpha;

import java.util.ArrayList;

import android.graphics.AvoidXfermode.Mode;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.Path;
import android.graphics.PorterDuffXfermode;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.location.Location;
import android.view.View;

public class DrawOnTop extends View implements SensorEventListener{

	Float azimut;
	
	float[] mValue;
	
	Paint mPaint = new Paint();
	private Path    mPath = new Path();
	private boolean mAnimate;
    private long    mNextTime;
    
    ArrayList<Building> targets;
	 
	PreviewActivity m_preActivity;
	
	public int idx;
	Location loc;
	Location loc_prime;
	
	Bitmap target;
	Bitmap left_arrow;
	Bitmap right_arrow;
	
	public DrawOnTop(PreviewActivity context) {
		super(context);
		m_preActivity = context;
		
		mPath.moveTo(0, -50);
        mPath.lineTo(-20, 60);
        mPath.lineTo(0, 50);
        mPath.lineTo(20, 60);
        mPath.close();
        
        targets = new ArrayList<Building>();
        loc = new Location("point");
        loc_prime = new Location("point_prime");
        
        setWillNotDraw(false);
        
        
        target = BitmapFactory.decodeResource(getResources(), R.drawable.target);
        left_arrow = BitmapFactory.decodeResource(getResources(), R.drawable.arrow_left);
        right_arrow = BitmapFactory.decodeResource(getResources(), R.drawable.arrow_right);
	}

  @Override 
  protected void onDraw(Canvas canvas) { 
          // TODO Auto-generated method stub 
          
	  double latitude = DBHolder.sharedHolder().getDB().get(idx).getlatitude();
	  double longitude = DBHolder.sharedHolder().getDB().get(idx).getlongitude();
	  
	  
	  
	  if(m_preActivity.m_last_location == null){
		  return;
	  }
	  
	  loc.setLatitude(latitude);
	  loc.setLongitude(longitude);
	  float dist = m_preActivity.m_last_location.distanceTo(loc);
	  
	  loc_prime.setLatitude(m_preActivity.m_last_location.getLatitude());
	  loc_prime.setLongitude(longitude);

	  float dist_y = loc.distanceTo(loc_prime);
	  
	  
	  double theta = (double) Math.acos(dist_y / dist);
	  theta = theta * 180.0f / Math.PI;
	  
	  //loc이 1사분면에 있을 때 : 그냥 theta
	  if(m_preActivity.m_last_location.getLatitude() <= loc.getLatitude() && m_preActivity.m_last_location.getLongitude() <= loc.getLongitude()){
		  
	  }
	  //loc이 2사분면에 있을 때 : 360 - theta
	  else if(m_preActivity.m_last_location.getLatitude() <= loc.getLatitude() && m_preActivity.m_last_location.getLongitude() > loc.getLongitude()){
		  theta = 360 - theta;
	  }
	  //loc이 3사분면에 있을 때 : theta + 180
	  else if(m_preActivity.m_last_location.getLatitude() > loc.getLatitude() && m_preActivity.m_last_location.getLongitude() > loc.getLongitude()){
		  theta = theta + 180;
	  }
	  //loc이 4사분면에 있을 때 : 180 - theta
	  else{
		  theta = 180 - theta;
	  }
	   
	  Paint paint = mPaint;
	  
	  paint.setXfermode(new PorterDuffXfermode(android.graphics.PorterDuff.Mode.DST_ATOP));

      canvas.drawColor(Color.TRANSPARENT);

      paint.setAntiAlias(true);
      paint.setColor(Color.RED);
      //paint.setStyle(Paint.Style.FILL);

      int w = canvas.getWidth();
      int h = canvas.getHeight();
      int cx = w / 2;
      int cy = h / 2;

      if (mValue != null) {      
    	  float xpos = 3* cx;
    	  
    	  float right_vision = (float)theta + 20.0f;
    	  float left_vision = (float)theta - 20.0f;
   
    	  
    	  boolean drawPoint = false;
    	//점.
    	  if(theta >= 20 && theta <= 340){
    		  if(mValue[0] > left_vision && mValue[0] < right_vision){
    			  
    			  xpos = (float)(-1 *  cx * mValue[0] / 20.0f + cx + (cx * theta) / 20.0f);
    			  //xpos = w * mValue[0] / (40.0f) - w * left_vision / (40.0f);
    			  //Log.e("asdas", "mvalue : " + mValue[0] + "   leftv " + left_vision + "  xpos " + xpos);
    			  canvas.drawCircle(xpos, cy, 50.0f, paint);
    			  //canvas.drawBitmap(target, xpos, cy, mPaint);
    			  drawPoint = true;
    		  }
    	  }
    	  else if(theta < 20){
    		  
    		  float val = mValue[0];
    		  if(val - 360 > left_vision){
    			  val = val - 360;
    			  xpos = (float)(cx * val / 20.0f + cx - (cx * theta) / 20.0f);
    			  canvas.drawCircle(xpos, cy, 50.0f, paint);
    			  //canvas.drawBitmap(target, xpos, cy, mPaint);
    			  drawPoint = true;
    		  }
    		  else if(val < right_vision){
    			  xpos = (float)(cx * val / 20.0f + cx - (cx * theta) / 20.0f);
    			  canvas.drawCircle(xpos, cy, 50.0f, paint);
    			  //canvas.drawBitmap(target, xpos, cy, mPaint);
    			  drawPoint = true;
    		  }
    	  }
    	  else if(theta > 340){
    		  float val = mValue[0];
    		  if(val + 360 < right_vision){
    			  val = val + 360;
    			  xpos = (float)(cx * val / 20.0f + cx - (cx * theta) / 20.0f);
    			  canvas.drawCircle(xpos, cy, 50.0f, paint);
    			  //canvas.drawBitmap(target, xpos, cy, mPaint);
    			  drawPoint = true;
    		  }
    		  else if(val > left_vision){
    			  xpos = (float)(cx * val / 20.0f + cx - (cx * theta) / 20.0f);
    			  canvas.drawCircle(xpos, cy, 50.0f, paint);
    			  //canvas.drawBitmap(target, xpos, cy, mPaint);
    			  drawPoint = true;
    		  }
    		  
    	  }
    	  
    	  paint.setColor(Color.WHITE);
    	  
    	  //화살표
    	  if(drawPoint == false){
    		  
    		  if(theta <= 180 && theta - mValue[0] >= -180 && theta - mValue[0] < 0){
    			  xpos = w * 0.9f;
        		  mPath.reset();
        		  mPath.moveTo(xpos, cy);
        	      mPath.lineTo(xpos - w * 0.12f, cy + h * 0.05f);
        	      mPath.lineTo(xpos - w * 0.12f, cy - h * 0.05f);
        	      mPath.close();
        	      canvas.drawBitmap(right_arrow, xpos, cy, paint);
        	      //canvas.drawPath(mPath, mPaint);
    		  }
    		  else if(theta <= 180){
    			  xpos = 0.0f;//w * 0.1f;
        		  mPath.reset();
        		  mPath.moveTo(xpos, cy);
        	      mPath.lineTo(xpos + w * 0.12f, cy + h * 0.05f);
        	      mPath.lineTo(xpos + w * 0.12f, cy - h * 0.05f);
        	      mPath.close();
        	      canvas.drawBitmap(left_arrow, xpos, cy, paint);
        	     // canvas.drawPath(mPath, mPaint);
    		  }
    		  
    		  
    		  else if(theta > 180 && theta - mValue[0] >= theta - 360 && theta - mValue[0] > 0){
    			  xpos = w * 0.9f;
        		  mPath.reset();
        		  mPath.moveTo(xpos, cy);
        	      mPath.lineTo(xpos - w * 0.12f, cy + h * 0.05f);
        	      mPath.lineTo(xpos - w * 0.12f, cy - h * 0.05f);
        	      mPath.close();
        	      canvas.drawBitmap(right_arrow, xpos, cy, paint);
        	      //canvas.drawPath(mPath, mPaint);
    		  }
    		  else if(theta > 180 && theta - mValue[0] >= 180 && theta - mValue[0] < theta){
    			  xpos = w * 0.9f;
        		  mPath.reset();
        		  mPath.moveTo(xpos, cy);
        	      mPath.lineTo(xpos - w * 0.12f, cy + h * 0.05f);
        	      mPath.lineTo(xpos - w * 0.12f, cy - h * 0.05f);
        	      mPath.close();
        	      canvas.drawBitmap(right_arrow, xpos, cy, paint);
        	      //canvas.drawPath(mPath, mPaint);
    		  }
    		  else if(theta > 180){
    			  xpos = 0.0f;//w * 0.1f;
        		  mPath.reset();
        		  mPath.moveTo(xpos, cy);
        	      mPath.lineTo(xpos + w * 0.12f, cy + h * 0.05f);
        	      mPath.lineTo(xpos + w * 0.12f, cy - h * 0.05f);
        	      mPath.close();
        	      canvas.drawBitmap(left_arrow, xpos, cy, paint);
        	      //canvas.drawPath(mPath, mPaint);
    		  }
    		  
    	  }
    	  
    	  paint.setTextSize(w * 0.05f);
    	  paint.setTextAlign(Align.CENTER);
    	  paint.setColor(Color.BLUE);
    	  canvas.drawText(dist + "m", w * 0.5f, h * 0.6f , paint);
    	  paint.setTextSize(w * 0.06f);
    	  paint.setColor(Color.GREEN);
    	  canvas.drawText(DBHolder.sharedHolder().getDB().get(idx).getName(), w * 0.5f, h * 0.4f, paint);
          //Log.e("sss", "azimuth : " + mValue[0] + "  pitch : " + mValue[1] + "  roll : " + mValue[2]);
      }
      //canvas.drawPath(mPath, mPaint);
      
      
      
  }

  
@Override
public void onAccuracyChanged(Sensor arg0, int arg1) {
	// TODO Auto-generated method stub
	
}

@Override
public void onSensorChanged(SensorEvent event) {
	// TODO Auto-generated method stub

    mValue = event.values;
    this.invalidate(); 
    
	/*
	if(event.sensor.getType() == Sensor.TYPE_ACCELEROMETER)
		mGravity = event.values;
	if(event.sensor.getType() == Sensor.TYPE_MAGNETIC_FIELD)
		mGeomagnetic = event.values;
	
	if (mGravity != null && mGeomagnetic != null) {
		 float R[] = new float[9];
		 float I[] = new float[9];
		 boolean success = SensorManager.getRotationMatrix(R, I, mGravity, mGeomagnetic);
		 
		 if (success) {
			 float orientation[] = new float[3];
			 SensorManager.getOrientation(R, orientation);
			 azimut = orientation[0]; // orientation contains: azimut, pitch and roll
			 
			 float azimuthInRadians = orientation[0];
				float azimuthInDegress = (float)Math.toDegrees(azimuthInRadians);
				if (azimuthInDegress < 0.0f) {
				    azimuthInDegress += 360.0f;
				}
				
				Log.e("azimut", azimuthInDegress + "");
			 
		 	}

		 
	 	}	
	 	
	this.invalidate();
	*/

}
}
